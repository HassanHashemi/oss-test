﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Lib
{
    public class UserInfo
    {
        private static Guid _hassanUserId = Guid.NewGuid();

        [Required(AllowEmptyStrings = false, ErrorMessage = "invalid name")]
        public string Name { get; set; }
        public Guid UserId { get; set; }

        public static UserInfo Me
        {
            get
            {
                return new UserInfo()
                {
                    Name = "Hassan",
                    UserId = _hassanUserId
                };
            }
        }
    }
}
