﻿using Lib;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Gmail.Controllers
{
    public class GmailAccountController : Controller
    {
        private readonly IDistributedCache _cache;

        public GmailAccountController(IDistributedCache cahe)
        {
            this._cache = cahe;
        }

        public ActionResult Login(string returnUrl)
        {
            return Redirect($"http://localhost:57208/account/SignIn?returnUrl=http://localhost:57250/GmailAccount/TryLogin?returnUrl={returnUrl}");
        }

        public async Task<ActionResult> TryLogin(string sessionId, string returnUrl)
        {
            var userData = await this._cache.GetStringAsync(sessionId);
            if (userData == null)
            {
                throw new Exception("User is not loggedIn");
            }

            var userInfo = JsonConvert.DeserializeObject<UserInfo>(userData);

            await this.SignInUser(userInfo);

            return Redirect(returnUrl);
        }

        private async Task SignInUser(UserInfo userInfo)
        {
            var claims = new[]
            {
                new Claim(ClaimTypes.Name, userInfo.Name),
                new Claim(ClaimTypes.NameIdentifier, userInfo.UserId.ToString()),
            };

            var identity = new ClaimsIdentity(
                claims, CookieAuthenticationDefaults.AuthenticationScheme);

            var principal = new ClaimsPrincipal(identity);

            await this.HttpContext.SignInAsync(principal);
        }
    }
}
