﻿using Lib;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Account.Controllers
{
    public class AccountController : Controller
    {
        private readonly IDistributedCache _distributedCache;

        public AccountController(IDistributedCache distributedCache)
        {
            this._distributedCache = distributedCache;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> SignIn(string returnUrl)
        {
            if (this.User.Identity.IsAuthenticated)
            {
                var userId = this.User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier);
                return Redirect($"{returnUrl}&sessionId={userId.Value}");
            }

            await this.SignInUser(UserInfo.Me);
            await _distributedCache.SetStringAsync(UserInfo.Me.UserId.ToString(), JsonConvert.SerializeObject(UserInfo.Me));
            returnUrl = $"{returnUrl}&sessionId={UserInfo.Me.UserId}";
            return Redirect(returnUrl);
        }

        private async Task SignInUser(UserInfo userInfo)
        {
            var claims = new[]
            {
                new Claim(ClaimTypes.Name, userInfo.Name),
                new Claim(ClaimTypes.NameIdentifier, userInfo.UserId.ToString())
            };

            var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            var principal = new ClaimsPrincipal(identity);

            await this.HttpContext.SignInAsync(principal);
        }
    }
}