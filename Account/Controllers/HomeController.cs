﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Account.Controllers
{
    public class SessionController : Controller
    {
        public async Task<ActionResult> Get(string key)
        {
            await this.HttpContext.Session.LoadAsync();

            var value = this.HttpContext.Session.GetString(key);

            return Content(value);
        }

        public async Task<ActionResult> Set(string key, string value)
        {
            this.HttpContext.Session.SetString(key, value);

            await HttpContext.Session.CommitAsync();

            return Ok("SET ");
        }
    }

    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
